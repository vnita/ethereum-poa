#!/bin/sh

ARGS_NUMBER="$#"
NODE_TYPE="$1"
OS=$(echo "$(uname -s|tr '[:upper:]' '[:lower:]')")
GIT_REPO="https://gitlab.com/vnita/ethereum-poa.git"
DOCKER_IMAGE="registry.gitlab.com/vnita/ethereum-poa"
USER="$(id -u)"
GROUP="$(id -g)"
if [ -f https_proxy ]; then
    HTTPS_PROXY=$(cat https_proxy)
    echo "Set https_proxy var: $HTTPS_PROXY"
    echo "Using https proxy..."
fi

if [ -f ip ]; then
    IP=$(cat ip)
    echo "Set IP var: $IP"
    EXTRA_CMD="--nat=extip:$IP"
    echo "Set extra param: $EXTRA_CMD"
fi

git() {
    echo "Executing GIT $1 $GIT_REPO "
    #docker run --rm --user $USER:$GROUP -v /$(pwd):/git alpine/git "$@"
    docker run --rm --user $USER:$GROUP -v /$(pwd):/git -e https_proxy=$HTTPS_PROXY --workdir "/$()/git" --entrypoint "git" $DOCKER_IMAGE "$@"
}

regularRun() {
    echo "regular node run"
    if [ $(echo "$OS" |sed 's/mingw64_nt.*/windows/') != "windows" ]; then
        ## LINUX/MAC: 
        docker run -d --user $USER:$GROUP --restart=always -p 30303:30303/tcp -p 30303:30303/udp -p 8545:8545 \
        -v "${PWD}/config/":/opt/ethereum/config/ \
        $DOCKER_IMAGE $EXTRA_CMD
    else 
        ## WINDOWS with git-bash
        docker run -d --user $USER:$GROUP -p 30303:30303/tcp -p 30303:30303/udp -p 8545:8545 \
        -v "/${PWD}/config/":/opt/ethereum/config/ \
        $DOCKER_IMAGE $EXTRA_CMD
    fi
}

sealerRun() {
    echo "Starting sealer node..."
    echo "$OS"
    # SO Verification
    if [ $(echo "$OS" |sed 's/mingw64_nt.*/windows/') != "windows" ]; then
        ## LINUX/MAC: 
        docker run -d --user $USER:$GROUP --restart=always -p 30303:30303/tcp -p 30303:30303/udp -p 8545:8545 \
        -v "${PWD}/data/":/opt/ethereum/.ethereum/ \
        -v "${PWD}/config/":/opt/ethereum/config/ \
        -v "${PWD}/account/":/opt/ethereum/account/  \
        -e NODE_TYPE="$NODE_TYPE" \
        $DOCKER_IMAGE $EXTRA_CMD
    else 
        ## WINDOWS with git-bash
        docker run -d --user $USER:$GROUP -p 30303:30303/tcp -p 30303:30303/udp -p 8545:8545 \
        -v "/${PWD}/config/":/opt/ethereum/config/ \
        -v "/${PWD}/data/":/opt/ethereum/.ethereum/ \
        -v "/${PWD}/account/":/opt/ethereum/account/ \
        -e TYPE=$NODE_TYPE \
        $DOCKER_IMAGE $EXTRA_CMD
    fi
}

## Validate if repo is up to date
if [ -d .git ]; then
    git fetch
    git reset --hard
    git pull
else
    git clone $GIT_REPO
    mv -f ./ethereum-poa/.git .
    rm -rf ethereum-poa
fi
docker pull $DOCKER_IMAGE

## Lets run the node
if [ $ARGS_NUMBER -gt 0 ]; then
    if [ "$NODE_TYPE" = "sealer" ]; then
        ## Some validations
        if ! [ -d "data" ]; then
            echo "data folder doesnt exist"
            mkdir data   
        fi
        if ! [ -d "account" ]; then
            echo "account folder doesnt exist"
            mkdir account
            ./scripts/genAccount.sh   
        fi 

        if ! [ -f "./account/account.txt" ]; then
            echo "account/account.txt file doesnt exist"  
            ./scripts/genAccount.sh
        fi

        if ! [ -f "./account/privatekey.txt" ]; then
            echo "account/privatekey.txt file doesnt exist"  
            ./scripts/genAccount.sh
        fi

        if ! [ -f "./account/password.txt" ]; then
            echo "account/password.txt file doesnt exist"  
            ./scripts/genAccount.sh
        fi
        
        sealerRun
    else
        echo "Usage: run.sh (optional) <sealer> "
        exit 1;
    fi
else 
    regularRun
fi




