# Ethereum POA

Andorran blockchain consortium network based on Ethereum with Clique Proof of Authority

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

## Tips
### Working behind a Proxy

If you are behind a proxy, you'll need to create a file named https_proxy with the proxy full url:

file: https_proxy
content:
```
https://<proxy-ip>:<proxy-port>
```
or:
```
http://<proxy-ip>:<proxy-port>
```

### Having an external IP diferent that outgoing proxy public/external IP
If you are behind a proxy and the IP used by that proxy to go to internet is diferent that your public IP that is configured with NAT for incoming connections, you'll need to create a file named ip with the external IP:

file: ip
content:
```
<public-ip>
```
for example:
```
123.456.789.100
```


## Prerequisites

Requirements:

* [Docker](https://docs.docker.com/install/) && User with privileges to execute docker containers

### 1st. Clone this repo in local folder:
##### Option 1.a: do it from terminal, if you have git installed simply clone the repo:
#

```
mkdir /some/path/vnitaFolder
```

```
cd /some/path/vnitaFolder
```
```
git clone https://gitlab.com/vnita/ethereum-poa.git
```

##### Option 1.b: do it from terminal, without git installed:
#
```
mkdir /some/path/vnitaFolder
```

```
cd /some/path/vnitaFolder
```

(If you're behind a proxy, create the var HTTPS_PROXY before)
```
docker run --rm -v /$(pwd):/git -u $(id -u):$(id -g) -e https_proxy="$HTTPS_PROXY" --workdir "/$()/git" --entrypoint "git" registry.gitlab.com/vnita/ethereum-poa clone https://gitlab.com/vnita/ethereum-poa.git
```

##### Option 2: download entire repo from gitLab repo
Follow the download link as zip folder && unzip where you want on your system
[Download](https://gitlab.com/vnita/ethereum-poa/-/archive/master/ethereum-poa-master.zip)

### Usage

To start a Regular Node:
```
cd ethereum-poa
./run.sh
```

To start a Sealer Node:
```
cd ethereum-poa
./run.sh sealer
```
* Follow the instructions on screen, it'll prompt to generate some files/folders if it's the 1st run
##### Note that:
* If it's not created, it will prompt to create directory account && some local files (pk, pass & account)
* Folders account && ethereum should be backed-up to mantain the same account for an accepted sealer

## Update files & configs
If some configuration from the network is updated by consortium consensus, you'll need to update your node also.
The update process is automated with this repo, all you need to do is:
--> Stop & remove running node on docker container
```
docker ps
```
* Find the id of the container that is running the node
```
docker stop <container-id>
docker rm <container-id>
```
--> Run a new container 
```
cd /some/path/vnitaFolder/ethereum-poa
./run.sh 
```
* Or if you're running a sealer
```
run.sh sealer
```

## Containers cleanup
It's recomended to regularly cleanup old containers 
* Be careful! This will delete any stopped container
```
docker container prune
```

If you prefear to do it manually:
```
docker ps -a
```
* Find and copy ids of all stopped containers you want to delete
```
docker rm <id1> <id2> <id3> ... <idN>
```

