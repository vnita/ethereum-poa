#!/bin/sh

CONFIG_FILE="config/config.toml"
PASS_FILE="account/password.txt"
PK_FILE="account/privatekey.txt"
ACCOUNT_FILE="account/account.txt"
ACCOUNT_FOLDER="account"
DOCKER_IMAGE="registry.gitlab.com/vnita/ethereum-poa"
USER="$(id -u)"
GROUP="$(id -g)"


newPassword() {
    clear
    question $PASS_FILE file
    echo "Generating new password..."
    docker run --rm --user $USER:$GROUP --entrypoint "ash" $DOCKER_IMAGE "./random.sh" > account/password.txt
}

newPrivateKey() {
    clear
    question $PK_FILE file
    echo "Generating new privatekey..."
    docker run --rm --user $USER:$GROUP --entrypoint "openssl" $DOCKER_IMAGE rand -hex 32 > account/privatekey.txt
    #docker run --rm --user $USER:$GROUP --entrypoint "ash" $DOCKER_IMAGE "./genPK.sh" > account/privatekey.txt       
}

newAccount() {
    clear
    question $ACCOUNT_FILE file
    echo "Importing account..."
    
    if ! tmp_file="$(mktemp)"; then >&2 echo Unable to create tmp file; fi
    echo $(docker run --rm --user $USER:$GROUP \
    -v /${PWD}/account/:/opt/ethereum/account/ \
    -v /${PWD}/config/:/opt/ethereum/config/ \
    --entrypoint "geth" $DOCKER_IMAGE -config $CONFIG_FILE account import --password $PASS_FILE $PK_FILE) > $tmp_file
    address="$(cut -d'{' -f2 "$tmp_file" | tr -d '}')"
    echo $address > account/account.txt   
}

newAccountFolder() {
    clear
    question $ACCOUNT_FOLDER folder
    echo "creating folder..."
    mkdir account
}


question() {
echo ""
		echo " ##    ALERT - You are going to create $1 $2 !!                     ##"
		echo " #                                                                                        #"
		echo " #                                                                                        #"
		echo " #     Please Verify that it doesn not already exsist before to continue                  #"
		echo " #                                                                                        #"
		echo " #                                                                                        #"
		echo " ## If not sure or you don't know what are we talking about... please press --> Ctrl+C   ##"
		echo ""
		while true; do
    			read -p " --> Do you really want to CREATE $1 $2 ? [Y/N] " yn
    			case $yn in
        			[Yy]* ) break;;
        			[Nn]* ) exit;;
        			* ) echo "Please answer yes or no.";;
    			esac
		done
}

echo "Generating account..."
## Check account folder
if ! [ -d "$ACCOUNT_FOLDER" ]; then
    echo "$ACCOUNT_FOLDER" folder not found
    newAccountFolder
    
else
    echo $ACCOUNT_FOLDER folder already exists - skipping step
fi
## Check pk file
if ! [ -f "$PK_FILE" ]; then
    echo Privatekey file "$PK_FILE" not found, creating...
    newPrivateKey
else
    echo $PK_FILE already exists - skipping step
fi

## Check password file
if ! [ -f "$PASS_FILE" ]; then
    echo Password file "$PASS_FILE" not found, creating...
    newPassword
else
    echo $PASS_FILE already exists - skipping step
fi

## Check account file
if ! [ -f "$ACCOUNT_FILE" ]; then
    echo Account address file "$ACCOUNT_FILE" not found, creating...
    newAccount
else
    echo $PASS_FILE already exists - skipping step
fi

