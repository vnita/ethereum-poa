#!/bin/sh

# Vars
## Routes
ETHEREUM="/opt/ethereum"
DATA_DIR="$ETHEREUM/.ethereum"
export HOME="$ETHEREUM"

## Files
CONFIG_FILE="$ETHEREUM/config/config.toml"
GENESIS_FILE="$ETHEREUM/config/genesis.json"
PASS_FILE="$ETHEREUM/account/password.txt"
PRIV_KEY="$ETHEREUM/account/privatekey.txt"
ACCOUNT_FILE="$ETHEREUM/account/account.txt"

## Constants
ANDORRAN_IPS="46.172.224.0/19,80.80.84.0/22,80.80.92.0/22,85.94.160.0/19,91.187.64.0/19,109.111.96.0/19,185.4.52.0/22,185.33.0.0/22,185.87.36.0/22,185.87.40.0/21,194.158.64.0/19,164.132.231.87/32"
OS=$(echo "$(uname -s|tr '[:upper:]' '[:lower:]')")

function regularChecks() {
        if ! [ -f "$CONFIG_FILE" ]; then
                echo Geth configuration file \"$CONFIG_FILE\" not found
                exit 1
        fi
        # # Check genesis exists
        if ! [ -f "$GENESIS_FILE" ]; then
                echo Geth configuration file \"$CONFIG_FILE\" not found
                exit 1
        fi
}

function sealerChecks() {
        # # Check key file
        if ! [ -f "$PRIV_KEY" ]; then
                echo Privatekey file \"$PRIV_KEY\" not found
                exit 1
        fi
        # # Check password file
        if ! [ -f "$PASS_FILE" ]; then
                echo PrivateKey password file \"$PASS_FILE\" not found
                exit 1
        fi
        # # Check account file
        if ! [ -f "$ACCOUNT_FILE" ]; then
                echo Account address file \"$ACCOUNT_FILE\" not found
                exit 1
        fi
}

function initChain() {
        if ! [ -d "$DATA_DIR/geth" ]; then
                echo "Data directory with genesis block not initalized yet."
                echo "Initializing blockchain..."
                geth --config "$CONFIG_FILE" init "$GENESIS_FILE" ||
                (echo "Could not initialize." && exit 1)
        fi
}

function importAccount() {
        echo "Importing account..."
        echo " - Command: geth --config <config.toml> account import <passFile> <keyFile>"
        geth --config "$CONFIG_FILE" --ipcdisable account import --password "$PASS_FILE" "$PRIV_KEY" ||
        (echo "Could not import account." && exit 1)
}

function gethRegular() {
        echo "Initializing geth..."
        echo " - Command: geth --config <config.toml> --netrestrict "$ANDORRAN_IPS"  "
        exec geth --config "$CONFIG_FILE" --netrestrict "$ANDORRAN_IPS" 
}

function gethSealer() {
        echo "Initializing sealer..."
        echo " - Command: geth --config <config.toml> --miner.etherbase <account> --unlock <account> --mine --password <passFile>"
        exec geth --config "$CONFIG_FILE" --netrestrict "$ANDORRAN_IPS" --miner.etherbase "$(cat $ACCOUNT_FILE)" --unlock "$(cat $ACCOUNT_FILE)" --mine --password "$PASS_FILE"
}

echo "Running as User:Group --> $(id -u):$(id -g)"

if [ $(echo "$NODE_TYPE") == "sealer" ]; then
        regularChecks
        sealerChecks
        initChain
        importAccount
        gethSealer
else 
        regularChecks
        initChain
        gethRegular
fi
